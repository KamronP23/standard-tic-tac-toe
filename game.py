def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board):
    print_board(board)
    print(board, "has won")
    print("GAME OVER")
    exit()

def is_row_winner(board, x):
    if board[0 + x] == board[1 + x] and board[1 + x] == board[2 + x]:
        return True

def is_column_winner(board, y):
    if board[0 + y] == board[3 + y] and board[3 + y] == board[6 + y]:
        return True

def is_diagnol_winner(board, z):
    if board[0 + z] == board[4] and board[4] == board[8 - z]:
        return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player


    if is_row_winner(board, 0):
        game_over(board[0])

    elif is_row_winner(board, 3):
        game_over(board[3])

    elif is_row_winner(board, 6):
        game_over(board[6])

    elif is_column_winner(board, 0):
        game_over(board[0])

    elif is_column_winner(board, 1):
        game_over(board[1])

    elif is_column_winner(board, 2):
        game_over(board[2])

    elif is_diagnol_winner(board, 0):
        game_over(board[0])

    elif is_diagnol_winner(board, 2):
        game_over(board[0])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")

# if board[0] == board[1] and board[1] == board[2]:
       # print_board(board)
        #print(board[0], "has won")
        #exit()

   # elif board[3] == board[4] and board[4] == board[5]:
       # print_board(board)
       # print(board[3], "has won")
       # exit()

    #elif board[6] == board[7] and board[7] == board[8]:
       # print_board(board)
       # print(board[6], "has won")
       # exit()
    #elif board[0] == board[3] and board[3] == board[6]:
        #game_over(board[0])
       # print_board(board)
       # print(board[0], "has won")
        #exit()
    #elif board[1] == board[4] and board[4] == board[7]:
        #game_over(board[1])
        #print_board(board)
        #print(board[1], "has won")
       # exit()
    #elif board[2] == board[5] and board[5] == board[8]:
        #game_over(board[2])
        #print_board(board)
        #print(board[2], "has won")
        #exit()
    #elif board[0] == board[4] and board[4] == board[8]:
        #game_over(board[0])
        #print_board(board)
        #print(board[0], "has won")
       # exit()
    #elif board[2] == board[4] and board[4] == board[6]:
        #game_over(board[0])
        #print_board(board)
        #print(board[0], "has won")
        #exit()